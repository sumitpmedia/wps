#!/bin/bash

# Exit if any command fails
set -e

# Change to the expected directory
cd "$(dirname "$0")"
cd ..


# Enable nicer messaging for build status
YELLOW_BOLD='\033[1;33m';
COLOR_RESET='\033[0m';
status () {
	echo -e "\n${YELLOW_BOLD}$1${COLOR_RESET}\n"
}

## Add Git Checks here
# Make sure there are no changes in the working tree.  Release builds should be
# traceable to a particular commit and reliably reproducible.  (This is not
# totally true at the moment because we download nightly vendor scripts).
changed=
if ! git diff --exit-code > /dev/null; then
	changed="file(s) modified"
elif ! git diff --cached --exit-code > /dev/null; then
	changed="file(s) staged"
fi
if [ ! -z "$changed" ]; then
	git status
	echo "ERROR: Cannot build plugin zip with dirty working tree."
	echo "       Commit your changes and try again."
	exit 1
fi

branch="$(git rev-parse --abbrev-ref HEAD)"
if [ "$branch" != 'master' ]; then
	echo "WARNING: You should probably be running this script against the"
	echo "         'master' branch (current: '$branch')"
	echo
	sleep 2
fi

# Start creating the build
mkdir -p release-package/wps
rsync -Rr \
	--exclude 'release-package' \
	--exclude 'vendor' \
	--exclude 'node_modules' \
	--exclude '.git' \
	--exclude 'bin' \
	--exclude 'package-lock.json' \
	--exclude 'notes.txt' \
	--exclude 'notes.md' \
	--exclude '.gitignore' \
	. release-package/wps/

cd release-package/wps/
echo "$(pwd)"
status "Installing dependencies..."
composer install --no-dev -o --no-scripts
cd ..

# Remove any existing zip file
rm -f wps.zip

# Generate the plugin zip file
status "Creating archive..."

zip -r wps.zip wps

# Remove directory
rm -rf wps

status "Done. Zip File is available inside release-package directory"